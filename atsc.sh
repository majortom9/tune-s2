CHANNEL=$1
ADAPTER="-adapter 1"
DAEMON="-daemon 1"
MODE="-mode dvr"
DVR="/dev/dvb/adapter1/dvr0"
POL="N"
SR=1000
# check if tune-s2 is already running, no need to start another one
# this may not work right if there is no / weak signal on the previous one
if ps aux | grep -q "[t]une-s2"; then
        echo "already running tune-s2,.."
# if not, go ahead and start a new tune-s2 process
else
        echo "/usr/local/bin/tune-s2 $CHANNEL $POL $SR $ADAPTER $DAEMON"
        /usr/local/bin/tune-s2 $CHANNEL $POL $SR $ADAPTER $DAEMON
fi
sleep 3
# check if demux is already running, no need to start another one
if ps aux | grep -q "[d]emux -adapter 1"; then  
	echo "already running demux.."
# if not, go ahead and start a new demux process
else 
	/usr/local/bin/demux $ADAPTER $MODE $DAEMON
fi
sleep 1
echo "/usr/bin/cvlc "$DVR $STREAM
/usr/bin/vlc $DVR 
/usr/bin/pkill tune-s2
/usr/bin/pkill demux
