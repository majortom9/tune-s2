#CC=arm-linux-gnueabi-gcc
CC=cc
CFLG= -g -W
SRC=kb.c diseqc.c tune-s2.c
HED=kb.h diseqc.h tune-s2.h
OBJ=kb.o diseqc.o tune-s2.o
BIND=/usr/local/bin/
INCLUDE=-I./include/dvb

TARGET=tune-s2
SCRIPTDIR=$(HOME)
all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(CFLG) $(OBJ) -o $(TARGET) $(CLIB) -lm -lgps -lpthread

$(OBJ): $(HED)

install: all
	cp $(TARGET) $(BIND)
	cp -a ./atsc.sh $(SCRIPTDIR)
	cp -a ./menu.sh $(SCRIPTDIR)
uninstall:
	rm $(BIND)$(TARGET)

clean:
	rm -f $(OBJ) $(TARGET) *~ ._*

%.o: %.c
	$(CC) $(CFLG) $(INCLUDE) -c $< -o $@
