#!/bin/bash
# A menu driven shell script sample template 
## ----------------------------------
# Step #1: Define variables
# ----------------------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'
 
# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}
 
one(){
	echo "one() called"
        $HOME/atsc.sh 7
}
 
# do something in two()
two(){
	echo "two() called"
        $HOME/atsc.sh 14
}
three(){
	echo "three() called"
        $HOME/atsc.sh 17
}
four(){
	echo "four() called"
        $HOME/atsc.sh 19
}
five(){
	echo "five() called"
        $HOME/atsc.sh 20
}
six(){
	echo "six() called"
        $HOME/atsc.sh 26
}
seven(){
	echo "seven() called"
        $HOME/atsc.sh 29
}
eight(){
	echo "eight() called"
        $HOME/atsc.sh 32
}
nine(){
	echo "nine() called"
        $HOME/atsc.sh 33
}
ten(){
	echo "ten() called"
        $HOME/atsc.sh 38
}
eleven(){
	echo "eleven() called"
        $HOME/atsc.sh 39
}
twelve(){
	echo "twelve() called"
        $HOME/atsc.sh 41
}
thirteen(){
	echo "thirteen() called"
        $HOME/atsc.sh 42
}
fourteen(){
	echo "fourteen() called"
        $HOME/atsc.sh 43
}
fifteen(){
	echo "fifteen() called"
        $HOME/atsc.sh 49
}

# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1.  tune ch 7"
	echo "2.  tune ch 14"
	echo "3   tune ch 17"
	echo "4.  tune ch 19"
	echo "5.  tune ch 20"
	echo "6.  tune ch 26"
	echo "7.  tune ch 29"
	echo "8.  tune ch 32"
	echo "9.  tune ch 33"
	echo "10. tune ch 38"
	echo "11. tune ch 39"
	echo "12. tune ch 41"
	echo "13. tune ch 42"
	echo "14. tune ch 43"
	echo "15. tune ch 49"
	echo "q. Exit"
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p "Enter choice [ 1 - 16] " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) three ;;
		4) four ;;
		5) five ;;
		6) six ;;
		7) seven ;;
		8) eight ;;
		9) nine ;;
		10) ten ;;
		11) eleven ;;		
		12) twelve ;;		
		13) thirteen ;;
		14) fourteen ;;
		15) fifteen ;;		
		q) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}
 
# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do
 
	show_menus
	read_options
done


